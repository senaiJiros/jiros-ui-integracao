import Vue from "vue";
import Router from "vue-router";
import { store } from "./store";
import Login from "./views/Login.vue";
import Pdf from "./views/pdf.vue";

// Administrador
import Dashboard from "./views/Administrador/Dashboard.vue";
import UploadForm from "./views/Administrador/UploadPlanilha/UploadForm.vue";
import ClienteForm from "./views/Clientes/ClienteForm.vue";
import CategoriaList from "./views/Administrador/Categorias/CategoriaList.vue";
import SubCategoriaList from "./views/Administrador/SubCategorias/SubCategoriaList.vue";
import ClasseList from "./views/Administrador/Classes/ClasseList.vue";
import ProdutoList from "./views/Administrador/Produtos/ProdutoList.vue";

// Solicitante
import DashboardS from "./views/Solicitante/Dashboard.vue";
import SolicitanteCotacaoForm from "@/views/Solicitante/Cotacao/CotacaoForm.vue";
import SolicitanteCotacaoList from "./views/Solicitante/Cotacao/CotacaoList.vue";
import SolicitanteCotacaoProdutosList from "./views/Solicitante/Cotacao/CotacaoProdutosList.vue";
import SolicitanteCotacaoUsuariosList from "./views/Solicitante/Cotacao/CotacaoUsuariosList.vue";
import MapaComparativo from "./views/Solicitante/Mapa/MapaComparativo.vue";

// Fornecedor
import DashboardF from "./views/Fornecedor/Dashboard.vue";
import FornecedorCotacaoList from "./views/Fornecedor/Cotacao/CotacaoList.vue";
import FornecedorCotacaoProdutosList from "./views/Fornecedor/Cotacao/CotacaoProdutosList.vue";

// Comum
import Erro404 from "./views/Status/Erro404.vue";
import DashboardUnico from "./views/Dashboard.vue";
import Documentacao from "./views/Comum/Documentacao.vue";
import CadastroUsuario from "./views/CadastroUsuario.vue";
import Convite from "./views/Comum/Todos/Convite.vue";
import RequisicaoProdutosList from "./views/Comum/FornecedorSolicitante/RequisicaoProdutosList.vue";

Vue.use(Router);

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/pdf",
      name: "pdf",
      component: Pdf
    },    
    {
      path: "/login",
      // isso funciona
      alias: ["/", "/home"],
      name: "login",
      component: Login
    },
    {
      path: "/cadastro",
      name: "CadastroUsuario",
      component: CadastroUsuario
    },
    {
      path: "/convite",
      name: "ConvidarUsuario",
      component: Convite,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/erro404",
      name: "erro404",
      component: Erro404
    },
    {
      path: "/dashboard",
      name: "dashboardUnico",
      component: DashboardUnico,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/administrador/dashboard",
      name: "dashboard",
      component: Dashboard,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    },
    {
      path: "/solicitante/dashboard",
      name: "dashboardS",
      component: DashboardS,
      meta: {
        requiresAuth: true,
        requesterAuth: true
      }
    },
    {
      path: "/fornecedor/dashboard",
      name: "dashboardF",
      component: DashboardF,
      meta: {
        requiresAuth: true,
        providerAuth: true
      }
    },
    {
      path: "/administrador/categorias",
      name: "CategoriaList",
      component: CategoriaList,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    },
    {
      path: "/administrador/subcategorias",
      name: "SubCategoriaList",
      component: SubCategoriaList,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    },
    {
      path: "/administrador/classes",
      name: "ClasseList",
      component: ClasseList,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    },
    {
      path: "/administrador/produtos",
      name: "ProdutoList",
      component: ProdutoList,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    },
    {
      path: "/administrador/planilha",
      name: "UploadForm",
      component: UploadForm,
      meta: {
        requiresAuth: true,
        adminAuth: true
      }
    },
    {
      path: "/solicitante/cotacao",
      name: "SolicitanteCotacaoForm",
      component: SolicitanteCotacaoForm,
      meta: {
        requiresAuth: true,
        requesterAuth: true
      }
    },
    {
      path: "/solicitante/cotacao/lista",
      name: "SolicitanteCotacaoList",
      component: SolicitanteCotacaoList,
      meta: {
        requiresAuth: true,
        requesterAuth: true
      }
    },
    {
      path: "/solicitante/cotacao/:id/produtos",
      name: "SolicitanteCotacaoProdutosList",
      component: SolicitanteCotacaoProdutosList,
      meta: {
        requiresAuth: true,
        requesterAuth: true
      }
    },
    {
      path: "/solicitante/cotacao/:id/usuarios",
      name: "SolicitanteCotacaoUsuariosList",
      component: SolicitanteCotacaoUsuariosList,
      meta: {
        requiresAuth: true,
        requesterAuth: true
      }
    },
    {
      path: "/solicitante/mapa-comparativo/:id",
      name: "MapaComparativo",
      component: MapaComparativo,
      meta: {
        requiresAuth: true,
        requesterAuth: true
      }
    },
    {
      path: "/fornecedor/cotacao/lista",
      name: "FornecedorCotacaoList",
      component: FornecedorCotacaoList,
      meta: {
        requiresAuth: true,
        providerAuth: true
      }
    },
    {
      path: "/requisicao/:id/produtos",
      name: "RequisicaoProdutosList",
      component: RequisicaoProdutosList,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/fornecedor/cotacao/:id/produtos",
      name: "FornecedorCotacaoProdutosList",
      component: FornecedorCotacaoProdutosList,
      meta: {
        requiresAuth: true,
        providerAuth: true
      }
    },
    {
      path: "/cliente/formulario",
      name: "clienteForm",
      component: ClienteForm,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/documentacao",
      name: "documentacao",
      component: Documentacao,
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (!to.matched.length) {
    next("/erro404");
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    const logado = store.getters.estaLogado;
    if (!logado) {
      next({
        name: "login"
      });
    } else if (to.meta.adminAuth) {
      const permission = store.getters.permissoes;
      let ehAdm = false;
      permission.forEach(element => {
        if (element.authority === "ADM") {
          ehAdm = true;
        }
      });
      if (ehAdm) {
        next();
      } else {
        next("/login");
      }
    } else if (to.meta.providerAuth) {
      const permission = store.getters.permissoes;
      let ehProvider = false;
      permission.forEach(element => {
        if (element.authority === "Provider") {
          ehProvider = true;
        }
      });
      if (ehProvider) {
        next();
      } else {
        next("/login");
      }
    } else if (to.meta.requesterAuth) {
      const permission = store.getters.permissoes;
      let ehRequester = false;
      permission.forEach(element => {
        if (element.authority === "Requester") {
          ehRequester = true;
        }
      });
      if (ehRequester) {
        next();
      } else {
        next("/login");
      }
    } else {
      next();
    }
  } else {
    next();
  }
});

// router.beforeEach((to, from, next) => {
//     if (to.matched.some(record => record.meta.requiresAuth)) {
//         if (store.getters.estaLogado) {
//             next()
//             return
//         }
//         next('/login')
//     } else {
//         next()
//     }
// });

export default router;
