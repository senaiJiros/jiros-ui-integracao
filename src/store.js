import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import api from "@/api";

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem("token") != null ? localStorage.getItem("token") : "",
        usuarioLogado: localStorage.getItem("token") != null ? (parseJwt(localStorage.getItem("token")).sub) : "",
        permissoes: localStorage.getItem("token") != null ? (parseJwt(localStorage.getItem("token")).permission) : "",
        nomeUsuario: localStorage.getItem("token") != null ? (parseJwt(localStorage.getItem("token")).name) : ""
    },
    getters: {
        fetchToken: state => state.token,
        estaLogado: state => !!state.token,
        usuarioLogado: state => state.usuarioLogado,
        permissoes: state => state.permissoes,
        nomeUsuario: state => state.nomeUsuario
    },
    mutations: {
        MODIFY_TOKEN: (state, payload) => {
            state.token = payload;


            state.usuarioLogado = (parseJwt(state.token).sub);
            state.permissoes = (parseJwt(state.token).permission);
            state.nomeUsuario = (parseJwt(state.token).name);


            localStorage.setItem("token", state.token)

            // state.usuarioLogado = parseJwt(state.token).sub
            // localStorage.setItem("tokenSub", state.usuarioLogado)

            // let p = [];
            // parseJwt(state.token).permission.forEach(element => {
            //     p.push(element.authority)
            // });            
            // localStorage.setItem("tokenPermission", p)
            // state.permissoes = p;

            // state.nomeUsuario = parseJwt(state.token).name
            // localStorage.setItem("tokenName", state.nomeUsuario)
        }
    },
    actions: {
        fetchToken: (context, payload) => {
            return api.postToken(payload)
                .then(r => {
                    if (r.token) {
                        context.commit('MODIFY_TOKEN', 'Bearer ' + r.token);
                        return r;
                    }
                })
                .catch(err => {
                    console.log(err);
                    return err
                })
        },
        fetchTokenSignUp: (context, payload) => {
            return api.postCadastrarUsuario(payload)
                .then(r => {
                    if (r.token) {
                        context.commit('MODIFY_TOKEN', 'Bearer ' + r.token);
                        return r;
                    }
                })
                .catch(err => {
                    console.log(err);
                    return err
                })
        },
        logout: () => {
            console.log('ops');
            localStorage.clear();

            store.replaceState({
                token: "",
                usuarioLogado: "",
                permissoes: "",
                nomeUsuario: ""
            });
        }

    }
})

function parseJwt(token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(window.atob(base64));
}
