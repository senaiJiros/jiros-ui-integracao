import Vue from "vue";
import axios from "axios";
import { store } from "./store";

const client = axios.create({
  // baseURL: 'http://192.168.4.158:8080/api/',
  // baseURL: 'http://172.16.6.35:8080/plataformajiros/api/',
  baseURL: "http://localhost:8080/api/",
  // baseURL: "http://192.168.4.76:8080/api/",
  json: true
});

export default {
  async execute(method, resource, data) {
    let accessToken = store.getters.fetchToken;
    return client({
      method,
      url: resource,
      data,
      headers: {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Teste" : "a",
        Authorization: `${accessToken}`
      }
    })
      .then(req => {
        return req.data;
      })
      .catch(err => {
        return err;
      });
  },
  async executePdf(method, resource, data) {
    let accessToken = store.getters.fetchToken;
    return client({
      method,
      url: resource,
      responseType:'blob',
      data,
      headers: {
        "Accept": "application/pdf",
        Authorization: `${accessToken}`
      }
    })
      .then(req => {
        return req.data;
      })
      .catch(err => {
        return null;
      });
  },
  getCategorias() {
    return this.execute("get", "/categories");
  },
  getPdf(id) {
    return this.executePdf("get", `/requests/${id}/end-quotation/pdf`);
  },
  getSubCategorias() {
    return this.execute("get", "/subcategories");
  },
  getClasses() {
    return this.execute("get", "/classes");
  },
  getProdutos() {
    return this.execute("get", "/products");
  },
  getUsuarios() {
    return this.execute("get", "/users");
  },
  getFabricantes() {
    return this.execute("get", "/manufacturers");
  },
  getFornecedores() {
    return this.execute("get", "/users/permission/provider");
  },
  postToken(data) {
    return this.execute("post", "/auth/jwt", data);
  },
  uploadImagem(imagem) {
    return this.execute("post", "/input", imagem);
  },
  postSolicitacaoCotacao(solicitacao) {
    return this.execute("post", "/requests", solicitacao);
  },
  patchConfirmarEntradaSolicitacaoCotacao(id) {
    return this.execute("patch", `/requests/${id}/user/confirm`);
  },
  patchRecusarEntradaSolicitacaoCotacao(id) {
    return this.execute("patch", `/requests/${id}/user/refuse`);
  },
  getDashboardAdmin() {
    return this.execute("get", "/dashboard/admin");
  },
  getDashboardSolicitante() {
    return this.execute("get", "/dashboard/requester");
  },
  getUsuarioPreCadastrado(usuarioComEmail) {
    return this.execute("post", "/users/email", usuarioComEmail);
  },
  getDashboardFornecedor() {
    return this.execute("get", "/dashboard/provider");
  },
  getSolicitacoesFornecedor() {
    return this.execute("get", "/requests/is-requester/false");
  },
  getSolicitacoesSolicitante() {
    return this.execute("get", "/requests/is-requester/true");
  },
  getProdutosCotacao(id) {
    return this.execute("get", `requests/${id}/product`);
  },
  getUsuariosCotacao(id) {
    return this.execute("get", `requests/${id}/user`);
  },
  postCotacao(data) {
    return this.execute("post", "/price-quote", data);
  },
  getMapaComparativo(id) {
    return this.execute("get", `/comparative-map/request/${id}`)
  },
  postFimDeRequisicao(data) {
    return this.execute("post", "/ending-quotation", data);
  },
  postCadastrarUsuario(usuario) {
    return this.execute("post", "/auth/signup", usuario)
  },
  postConvidarUsuario(usuario) {
    return this.execute("post", "/users/invite", usuario)
  }
  // get para buscar as permissoes
  // post recebendo os dados (pra salvar)

};
